using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    public static GameManager instance;

    public GameObject PlayerGO;

    public AudioSource OverworldBGM;
    public AudioSource DungeonBGM;
    public AudioSource Attack;

    private int score;
    public bool playerAlive;
    private int playerHealth;

    public void PlayAttack()
    {
        Attack.Play();
    }

    public void PlayOverworld()
    {
        OverworldBGM.Play();
    }

    public void PlayDungeon()
    {
        DungeonBGM.Play();
    }

    public void StopOverworld()
    {
        OverworldBGM.Stop();
    }

    public void StopDungeon()
    {
        DungeonBGM.Stop();
    }

    public void SetHealth(int _playerHealth)
    {
        playerHealth = _playerHealth;
        UIManager.instance.UpdateHealth();
    }

    public int GetScore()
    {
        return score;
    }

    public int GetHealth()
    {
        return playerHealth;
    }

    public bool GetPlayerAlive()
    {
        return playerAlive;
    }

    public void AddScore(int _score)
    {
        score += _score;
        UIManager.instance.UpdateScore();
    }

    private void Awake()
    {
        if (instance != null && instance != this)
        {
            Destroy(gameObject);
        }
        else
        {
            instance = this;
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        playerAlive = true;
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            Application.Quit();
        }
    }
}
