using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HealthLogic : MonoBehaviour
{
    [SerializeField] public int maxHealth;
    private EnemyLogic el;
    private PlayerLogic pl;
    public int currentHealth;

    public void TakeDamage(int damageTaken)
    {
        Debug.Log("Damage Taken! " + damageTaken);
        currentHealth -= damageTaken;
        if(el != null)
        {
            el.DeathCheck();
        }
        if(pl != null)
        {
            pl.DeathCheck();
        }

    }

    // Start is called before the first frame update
    void Start()
    {
        el = gameObject.GetComponent<EnemyLogic>();
        pl = gameObject.GetComponent<PlayerLogic>();
        currentHealth = maxHealth;
    }

    // Update is called once per frame
    void Update()
    {

    }
}
