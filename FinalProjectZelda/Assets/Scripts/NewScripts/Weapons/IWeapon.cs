using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IWeapon
{
    public void Attack();
    public void AttackEnd();
    GameObject GetGameObject();
    GameObject GetAttackObject();
}
