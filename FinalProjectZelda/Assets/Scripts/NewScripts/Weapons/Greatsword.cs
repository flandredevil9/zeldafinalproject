using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Greatsword : MonoBehaviour, IWeapon
{
    private int attackValue;
    [SerializeField] GreatswordItem item;
    Greatsword createdWeapon;
    // Start is called before the first frame update
    void Start()
    {
        attackValue = 2;
    }

    // Update is called once per frame
    void Update()
    {

    }

    private void OnTriggerEnter(Collider other)
    {
        EnemyLogic el = other.GetComponent<EnemyLogic>();
        if (el != null)
        {
            el.healthLogic.TakeDamage(attackValue);
        }
    }

    public void Attack()
    {
        Debug.Log("Greatsword Attack");
        //createdWeapon = Instantiate(this, pl.transform.position, Quaternion.Euler(90, pl.transform.rotation.eulerAngles.y, 0));
    }

    public GameObject GetGameObject()
    {
        return item.gameObject;
    }

    public void AttackEnd()
    {
        Debug.Log("Destroyed " + createdWeapon);
        Destroy(createdWeapon);
    }

    public GameObject GetAttackObject()
    {
        return this.gameObject;
    }
}
