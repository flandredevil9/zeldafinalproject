using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GreatswordItem : MonoBehaviour
{
    [SerializeField] public Greatsword weapon;
    private void OnTriggerStay(Collider other)
    {
        PlayerLogic pl = other.GetComponent<PlayerLogic>();

        if (pl != null && pl.currentWeapon == null && Input.GetKey(KeyCode.E))
        {
            pl.PickupWeapon(weapon);
            Destroy(this.gameObject);
        }
    }
}
