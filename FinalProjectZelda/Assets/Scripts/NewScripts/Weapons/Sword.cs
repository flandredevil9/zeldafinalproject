using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Sword : MonoBehaviour, IWeapon
{
    private int attackValue;
    [SerializeField] SwordItem item;
    Sword createdWeapon;
    // Start is called before the first frame update
    void Start()
    {
        attackValue = 1;
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerEnter(Collider other)
    {
        Debug.Log("Trigger on enemy");
        EnemyLogic el = other.GetComponent<EnemyLogic>();
        if (el != null)
        {
            Debug.Log("Trigger on enemy");
            el.healthLogic.TakeDamage(attackValue);
        }
    }

    public void Attack()
    {
        Debug.Log("Sword Attack");
        //createdWeapon = Instantiate(this, pl.transform.position, Quaternion.Euler(90, pl.transform.rotation.eulerAngles.y, 0));
    }

    public GameObject GetGameObject()
    {
        return item.gameObject;
    }

    public void AttackEnd()
    {
        Debug.Log("Destroyed " + createdWeapon);
        Destroy(createdWeapon);
    }

    public GameObject GetAttackObject()
    {
        return this.gameObject;
    }
}
