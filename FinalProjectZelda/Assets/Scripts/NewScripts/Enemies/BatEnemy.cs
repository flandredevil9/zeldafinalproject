using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BatEnemy : MonoBehaviour, IEnemy, IPlayerDamageable
{
    public int attackValue;
    public int scoreValue = 1;

    // Start is called before the first frame update
    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {
        Move();
    }
    public void Attack()
    {
        Debug.Log("Bat Attacks");
    }

    public GameObject GetGameObject()
    {
        return this.gameObject;
    }

    public void Move()
    {
        //Debug.Log("Octorok Moves");
    }

    public int DealDamage()
    {
        return attackValue;
    }

    public int GetScoreValue()
    {
        return scoreValue;
    }
}
