using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyLogic : MonoBehaviour
{
    //public int currentDirection;
    //public int lastDirection1;
    //public int lastDirection2;
    public HealthLogic healthLogic;
    public IEnemy enemyType;
    //public int maxHealth;
    //public int currentHealth;

    // Start is called before the first frame update
    void Start()
    {
        enemyType = gameObject.GetComponent<IEnemy>();
        healthLogic = gameObject.GetComponent<HealthLogic>();
        //currentDirection = 0;
        //lastDirection1 = 0;
        //lastDirection2 = 0;
        //maxHealth = 1;
        //currentHealth = maxHealth;
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    public void DeathCheck()
    {
        if (healthLogic.currentHealth <= 0)
        {
            Debug.Log("Octorok has died!");
            GameManager.instance.AddScore(enemyType.GetScoreValue());
            Destroy(this.gameObject);
        }
    }
}
