using System.Collections;
using System.Collections.Generic;
using UnityEngine;
public class TrackingCamera : MonoBehaviour
{
    [SerializeField] private List<Transform> trackObj;
    [SerializeField] private Vector3 com;
    [SerializeField] private Vector3 vOffset;
    [SerializeField] private Vector3 vZoomInWindow;
    [SerializeField] private Vector3 vZoomOutWindow;
    public float zoomOutSpeed = default;
    public float zoomInSpeed = default;
    private Camera cam;
    // Start is called before the first frame update
    void Start()
    {
        cam = this.GetComponent<Camera>();
        zoomInSpeed = 1.1f * zoomOutSpeed;
    }
    private bool _UpdateZoomIn()
    {
        bool rval = false;
        // make sure all trackObj are onscreen
        foreach (Transform pt in trackObj)
        {
            Vector3 p = cam.WorldToViewportPoint(pt.position);
            if ((p.x > vZoomInWindow.x) && (p.x < (1.0f - vZoomInWindow.x)))
            {
                if ((p.y > vZoomInWindow.y) && (p.y < (1.0f - vZoomInWindow.y)))
                {
                    rval = true;
                }
            }
        }
        return rval;
    }
    private bool _UpdateZoomOut()
    {
        bool rval = false;
        foreach (Transform pt in trackObj)
        {
            Vector3 p = cam.WorldToViewportPoint(pt.position);
            if ((p.x < vZoomOutWindow.x) || (p.x > (1.0f - vZoomOutWindow.x)))
            {
                rval = true;
            }
            if ((p.y < vZoomOutWindow.y) || (p.y > (1.0f - vZoomOutWindow.y)))
            {
                rval = true;
            }
        }
        return rval;
    }
    private void TrackingCameraUpdate()
    {
        // compute the center of mass
        foreach (Transform t in trackObj)
            com += t.position;
        com /= trackObj.Count;
        // make zoom decision
        bool zoomOut = _UpdateZoomOut();
        bool zoomIn = _UpdateZoomIn();
        if (zoomOut)
            vOffset.y += zoomOutSpeed * Time.deltaTime;
        else
            vOffset.y -= zoomInSpeed * Time.deltaTime;
        // update zoom and lookat
        this.transform.position = com + vOffset;
        this.transform.LookAt(com);
    }
    // Update is called once per frame
    void Update()
    {
        TrackingCameraUpdate();
    }
}