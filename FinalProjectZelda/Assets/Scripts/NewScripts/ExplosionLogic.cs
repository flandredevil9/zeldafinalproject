using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ExplosionLogic : MonoBehaviour
{

    //private void OnTriggerEnter(Collider other)
    //{
    //    Debug.Log("ok");
    //    BreakableWallLogic bw = other.gameObject.GetComponent<BreakableWallLogic>();
    //    if (bw != null)
    //    {
    //        Debug.Log("ok");
    //        Destroy(other.gameObject);
    //    }
    //}

    private void OnTriggerEnter(Collider other)
    {
        Debug.Log("Trigger on enemy");
        BreakableWallLogic bw = other.GetComponent<BreakableWallLogic>();
        if (bw != null)
        {
            Debug.Log("Trigger on enemy");
            Destroy(bw.gameObject);
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(BlowUp());
    }
    IEnumerator BlowUp()
    {

        yield return new WaitForSeconds(0.5f);
        Destroy(this.gameObject);
    }
    // Update is called once per frame
    void Update()
    {
        
    }
}
