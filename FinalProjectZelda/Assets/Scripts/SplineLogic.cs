using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SplineLogic : MonoBehaviour
{
    public List<GameObject> ControlPoints;
    public float dt = 0.01f;
    private float t = 0.0f;
    private int nHead = 0;
    public GameObject HeadObj;
    public bool isLooping = false;

    // Use this for initialization
    void Start()
    {
        nHead = 0;
    }

    Vector3 PointOnCurve(float _t, Vector3 p0, Vector3 p1, Vector3 p2, Vector3 p3)
    {
        //
        //	The spline passes through all of the control points.
        //	The spline is C1 continuous, meaning that there are no discontinuities in the tangent direction and magnitude.
        //	The spline is not C2 continuous.  The second derivative is linearly interpolated within each segment, causing the curvature to vary linearly over the length of the segment.
        //	Points on a segment may lie outside of the domain of P1 -> P2.
        Vector3 vOut = new Vector3(0.0f, 0.0f, 0.0f);

        float t2 = _t * _t;
        float t3 = t2 * _t;

        vOut.x = 0.5f * ((2.0f * p1.x) + (-p0.x + p2.x) * _t + (2.0f * p0.x - 5.0f * p1.x + 4 * p2.x - p3.x) * t2 +
                    (-p0.x + 3.0f * p1.x - 3.0f * p2.x + p3.x) * t3);

        vOut.y = 0.5f * ((2.0f * p1.y) + (-p0.y + p2.y) * _t + (2.0f * p0.y - 5.0f * p1.y + 4 * p2.y - p3.y) * t2 +
                    (-p0.y + 3.0f * p1.y - 3.0f * p2.y + p3.y) * t3);

        vOut.z = 0.5f * ((2.0f * p1.z) + (-p0.z + p2.z) * _t + (2.0f * p0.z - 5.0f * p1.z + 4 * p2.z - p3.z) * t2 +
                    (-p0.z + 3.0f * p1.z - 3.0f * p2.z + p3.z) * t3);

        return vOut;
    }

    // Fixing index
    private int ClampCPIndex(int pos)
    {
        if (pos < 0)
        {
            pos = ControlPoints.Count - 1;
        }

        if (pos > ControlPoints.Count)
        {
            pos = 1;
        }
        else if (pos > ControlPoints.Count - 1)
        {
            pos = 0;
        }

        return pos;
    }

    public void SplineLogicUpdate()
    {
        // update state
        t += dt;

        // wrap when t exceeds end of curve segment
        if (t > 1.0f)
        {
            t -= 1.0f;
            nHead++;
        }

        // wrap nHead once we reach end
        if (isLooping && nHead == (ControlPoints.Count - 3))
        {
            t = 0.0f;
            nHead = 0;
        }

        // extract interpolated point from spline
        if (ControlPoints.Count > 0 && isLooping ? true : nHead + 3 <= ControlPoints.Count)
        {
            Vector3 vOut = new Vector3(0.0f, 0.0f, 0.0f);

            Vector3 p0 = ControlPoints[ClampCPIndex(nHead)].transform.position;
            Vector3 p1 = ControlPoints[ClampCPIndex(nHead + 1)].transform.position;
            Vector3 p2 = ControlPoints[ClampCPIndex(nHead + 2)].transform.position;
            Vector3 p3 = ControlPoints[ClampCPIndex(nHead + 3)].transform.position;

            vOut = PointOnCurve(t, p0, p1, p2, p3);

            if (HeadObj)
                HeadObj.transform.position = vOut;
        }
    }

    // Update is called once per frame
    void Update()
    {
        SplineLogicUpdate();
    }
}
