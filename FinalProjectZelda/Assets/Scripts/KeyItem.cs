using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KeyItem : MonoBehaviour
{
    private void OnTriggerStay(Collider other)
    {
        PlayerLogic pl = other.GetComponent<PlayerLogic>();

        if (pl != null)
        {
            pl.keys++;
            Destroy(gameObject);
        }
    }
}