using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerDropWeapon : MonoBehaviour
{
    PlayerLogic pl;
    // Start is called before the first frame update
    void Start()
    {
        pl = GetComponent<PlayerLogic>();
    }

    // Update is called once per frame
    void Update()
    {
        
        if(Input.GetKeyDown(KeyCode.Q) && pl.hasWeapon && !pl.isAttacking)
        {

            Debug.Log("Weapon Dropped" + pl.currentWeapon.GetGameObject().name);
            Instantiate(pl.currentWeapon.GetGameObject(), pl.transform.position, Quaternion.Euler(90, pl.transform.rotation.eulerAngles.y, 0));
            pl.hasWeapon = false;
            pl.currentWeapon = null;
        }

    }
}