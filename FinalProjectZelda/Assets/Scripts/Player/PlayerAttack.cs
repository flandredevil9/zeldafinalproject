using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerAttack : MonoBehaviour
{
    private PlayerLogic pl;
    private GameObject weaponInstance;
    // Start is called before the first frame update
    void Start()
    {
        pl = gameObject.GetComponent<PlayerLogic>();
    }

    // Update is called once per frame
    void Update()
    {
        if(pl.hasWeapon && Input.GetKeyDown(KeyCode.Space))
        {
            Attack();
        }
        if(pl.hasWeapon && Input.GetKeyUp(KeyCode.Space))
        {
            AttackEnd();
        }
    }
    public void Attack()
    {
        GameManager.instance.PlayAttack();
        pl.isAttacking = true;
        weaponInstance = Instantiate(pl.currentWeapon.GetAttackObject(), pl.transform.position + pl.transform.forward * 1.5f + new Vector3(0,-0.5f, 0), Quaternion.Euler(90, pl.transform.rotation.eulerAngles.y, 0));
        
        //pl.currentWeapon.Attack(pl);
    }
    public void AttackEnd()
    {
        pl.isAttacking = false;
        Destroy(weaponInstance);
        //pl.currentWeapon.AttackEnd();
    }
}