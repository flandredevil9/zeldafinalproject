using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerLogic : MonoBehaviour
{
    public bool hasWeapon = false;
    public IWeapon currentWeapon;
    public bool isAttacking = false;
    [SerializeField] private HealthLogic healthLogic;
    public float invincibilityDuration;
    public bool isInvincible = false;
    public bool bombs = false;
    [SerializeField] public GameObject bomb;
    public uint keys = 0; // Ammount of universal keys that player has. Can open any door (like in TBOI)

    private void Start()
    {
        healthLogic = gameObject.GetComponent<HealthLogic>();
        Debug.Log(healthLogic.currentHealth);
        GameManager.instance.SetHealth(healthLogic.currentHealth);
        hasWeapon = false;

        if (GameManager.instance.PlayerGO == null)
            GameManager.instance.PlayerGO = gameObject;
    }
    private void Update()
    {
        if(bombs && Input.GetKeyDown(KeyCode.R))
        {
            Instantiate(bomb, transform.position + transform.forward * 1.5f + new Vector3(0, -0.5f, 0), Quaternion.Euler(90, transform.rotation.eulerAngles.y, 0));
        }
    }
    private void OnCollisionEnter(Collision collision)
    {
        Debug.Log("Collision");

        // Enemy Collision
        IPlayerDamageable pd = collision.gameObject.GetComponent<IPlayerDamageable>();
        if(pd != null && !isInvincible)
        {
            Debug.Log("Player has taken damage");
            healthLogic.TakeDamage(pd.DealDamage());
            GameManager.instance.SetHealth(healthLogic.currentHealth);
            StartCoroutine(InvincibilityTimer());
        }

        // Door Collision
        DoorLogic dl = collision.gameObject.GetComponentInParent<DoorLogic>();
        if(dl != null)
        {
            if (dl.locked)
            {
                if(keys > 0)
                {
                    keys--;
                    dl.locked = false;
                    Debug.Log("Door is now unlocked");
                    dl.DoorEnter();
                }
                else
                {
                    Debug.Log("Door is locked");
                }
            }
            else
            {
                dl.DoorEnter();
            }
        }

        //Bomb Item Collision
        BombItem bi = collision.gameObject.GetComponent<BombItem>();
        if(bi != null)
        {
            bombs = true;
            Destroy(bi.gameObject);
        }

    }
    IEnumerator InvincibilityTimer()
    {
        Debug.Log("Player is invincible");
        isInvincible = true;
        yield return new WaitForSeconds(invincibilityDuration);
        isInvincible = false;
        Debug.Log("Player is no longer invincible");
    }
    public void PickupWeapon(IWeapon weapon)
    {
        hasWeapon = true;
        currentWeapon = weapon;
    }
    public void DeathCheck()
    {
        if (healthLogic.currentHealth <= 0)
        {
            Debug.Log("Player has died!");
            Debug.Log("GAME OVER");
            UIManager.instance.GameOver();
            Destroy(this.gameObject);
        }
    }
}
