using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    private PlayerLogic pl;
    [SerializeField] float movementSpeed;   
    private Vector3 movementVector;

    void Start()
    {
        pl = gameObject.GetComponent<PlayerLogic>();
        movementSpeed = 5;
    }

    void Update()
    {
        CalculateMovement();
        Move();   
    }

    void CalculateMovement()
    {
        if(Input.GetKey(KeyCode.W) && !pl.isAttacking)
        {
            movementVector += new Vector3(0,0,1);
            transform.root.localRotation = Quaternion.Euler(0,0,0);
        }
        if (Input.GetKey(KeyCode.A) && !pl.isAttacking)
        {
            movementVector += new Vector3(-1, 0, 0);
            transform.root.localRotation = Quaternion.Euler(0, -90, 0);
        }
        if (Input.GetKey(KeyCode.S) && !pl.isAttacking)
        {
            movementVector += new Vector3(0, 0, -1);
            transform.root.localRotation = Quaternion.Euler(0, 180, 0);
        }
        if (Input.GetKey(KeyCode.D) && !pl.isAttacking)
        {
            movementVector += new Vector3(1, 0, 0);
            transform.root.localRotation = Quaternion.Euler(0, 90, 0);
        }
    }

    void Move()
    {
        transform.Translate(movementVector * movementSpeed * Time.deltaTime, Space.World);
        movementVector = new Vector3(0,0,0);
    }


}
