using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIManager : MonoBehaviour
{
    public static UIManager instance;

    [SerializeField] public GameObject mainCanvas;
    [SerializeField] public GameObject titleScreenPanel;
    [SerializeField] public GameObject gameOverPanel;
    [SerializeField] public GameObject HUDPanel;
    [SerializeField] public GameObject howToPlayPanel;

    //Gameplay HUD
    [SerializeField] public Text scoreHUDText;
    [SerializeField] public Text healthHUDText;

    //Game Over
    [SerializeField] public Text scoreGameOverText;

    private void Awake()
    {
        if (instance == null)
            instance = this;
        mainCanvas.SetActive(true);
        titleScreenPanel.SetActive(true);
        howToPlayPanel.SetActive(false);
        gameOverPanel.SetActive(false);
        HUDPanel.SetActive(false);
    }
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void UpdateScore()
    {
        scoreHUDText.text = "Score: " + GameManager.instance.GetScore();
    }
    public void UpdateHealth()
    {
        healthHUDText.text = "Health: " + GameManager.instance.GetHealth();
    }
    public void GameOver()
    {
        HUDPanel.SetActive(false);
        gameOverPanel.SetActive(true);
        scoreGameOverText.text = "Final Score: " + GameManager.instance.GetScore();
    }
    public void StartGameButton()
    {
        titleScreenPanel.SetActive(false);
        HUDPanel.SetActive(true);
        GameManager.instance.PlayOverworld();
        scoreHUDText.text = "Score: " + GameManager.instance.GetScore();
        healthHUDText.text = "Health: " + GameManager.instance.GetHealth();
    }
    public void HowToPlayBackButton()
    {
        titleScreenPanel.SetActive(true);
        howToPlayPanel.SetActive(false);
    }
    public void HowToPlayButton()
    {
        titleScreenPanel.SetActive(false);
        howToPlayPanel.SetActive(true);
    }
    public void RestartGameButton()
    {
        Debug.Log("This would reset the game.");
    }
}
