using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BombLogic : MonoBehaviour
{
    [SerializeField] GameObject explosion;
    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(BlowUp());
    }


    IEnumerator BlowUp()
    {

        yield return new WaitForSeconds(0.5f);
        Instantiate(explosion, transform.position, Quaternion.Euler(90, transform.rotation.eulerAngles.y, 0));
        Destroy(this.gameObject);
    }
}
