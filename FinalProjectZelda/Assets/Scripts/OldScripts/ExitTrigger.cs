using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ExitTrigger : MonoBehaviour
{
    [SerializeField] public bool hasTriggered = default;
    public Collider objCollider;

    private void OnTriggerExit(Collider other)
    {
        hasTriggered = true;
        objCollider = other;
        //tec.OnTriggerEnter(other);
    }
}
