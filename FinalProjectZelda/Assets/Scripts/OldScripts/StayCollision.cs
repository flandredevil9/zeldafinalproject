using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StayCollision : MonoBehaviour
{
    [SerializeField] public bool hasCollided = default;
    public Collision objCollision;
    private void OnCollisionStay(Collision collision)
    {
        hasCollided = true;
        objCollision = collision;
    }
}
