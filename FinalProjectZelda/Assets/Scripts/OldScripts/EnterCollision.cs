using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnterCollision : MonoBehaviour
{
    [SerializeField] public bool hasCollided = default;
    public Collision objCollision;
    private void OnCollisionEnter(Collision collision)
    {
        hasCollided = true;
        objCollision = collision;
    }
}
