using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StayTrigger : MonoBehaviour
{
    [SerializeField] public bool hasTriggered = default;
    public Collider objCollider;

    private void OnTriggerStay(Collider other)
    {
        hasTriggered = true;
        objCollider = other;
        //tec.OnTriggerEnter(other);
    }
}
