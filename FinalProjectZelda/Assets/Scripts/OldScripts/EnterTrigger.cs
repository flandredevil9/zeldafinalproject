using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnterTrigger : MonoBehaviour
{
    [SerializeField] public bool hasTriggered = default;
    public List<Collider> objColliders;

    private void OnTriggerEnter(Collider other)
    {
        hasTriggered = true;
        objColliders.Add(other);
    }
}
