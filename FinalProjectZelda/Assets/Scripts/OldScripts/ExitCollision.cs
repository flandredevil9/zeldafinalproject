using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ExitCollision : MonoBehaviour
{
    [SerializeField] public bool hasCollided = default;
    public Collision objCollision;
    private void OnCollisionExit(Collision collision)
    {
        hasCollided = true;
        objCollision = collision;
    }
}
