using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DoorLogic : MonoBehaviour
{
    [SerializeField] private GameObject playerGO;
    [SerializeField] private DoorLogic exitDoor;
    [SerializeField] private GameObject doorTP;
    [SerializeField] private Camera cameraLogic;
    [SerializeField] private GameObject cameraWayPoint;
    public bool locked = false;

    /// <summary>
    ///  Scipt of player ENTERING through the door
    /// </summary>
    public void DoorEnter()
    {
        if (GetPlayerGO() != null)
        {
            // Do stuff pre teleport here

            exitDoor.DoorExit();
        }
    }

    /// <summary>
    ///  Scipt of player EXITING through the door
    /// </summary>
    public void DoorExit() 
    {
        if(GetPlayerGO() != null)
        {
            locked = false;
            playerGO.transform.position = doorTP.transform.position;
            cameraLogic.transform.position = cameraWayPoint.transform.position;
        }
    }

    private GameObject GetPlayerGO()
    {
        if (playerGO == null && GameManager.instance.PlayerGO != null)
            playerGO = GameManager.instance.PlayerGO;

        return playerGO;
    }

    // Start is called before the first frame update
    void Start()
    {
        if (    exitDoor == null 
            ||  doorTP == null 
            ||  cameraWayPoint == null
            )
            Destroy(gameObject);

        if (cameraLogic == null)
            cameraLogic = Camera.main;

        GetPlayerGO();
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
