using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TriggerExitCondition : Condition
{
    public override bool Eval(GameObject recipient)
    {
        bool rval = false;
        if (recipient.GetComponent<ExitTrigger>().hasTriggered)
        {
            rval = true;
            recipient.GetComponent<ExitTrigger>().hasTriggered = false;

        }
        return rval;
    }
}
