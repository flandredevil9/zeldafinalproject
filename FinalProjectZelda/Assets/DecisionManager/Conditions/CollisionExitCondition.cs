using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CollisionExitCondition : Condition
{
    public override bool Eval(GameObject recipient)
    {

        bool rval = false;
        if (recipient.GetComponent<ExitCollision>().hasCollided)
        {
            rval = true;
            recipient.GetComponent<ExitCollision>().hasCollided = false;
        }
        return rval;
    }
}
