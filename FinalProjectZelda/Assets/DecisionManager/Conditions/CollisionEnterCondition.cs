using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CollisionEnterCondition : Condition
{
    

    public override bool Eval(GameObject recipient)
    {

        bool rval = false;
        if (recipient.GetComponent<EnterCollision>().hasCollided)
        {
            rval = true;
            recipient.GetComponent<EnterCollision>().hasCollided = false;
        }
        return rval;
    }
}
