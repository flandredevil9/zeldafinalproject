using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KeyDownCondition : Condition
{
    [SerializeField] private KeyCode key;
    public override bool Eval(GameObject recipient)
    {
        bool rval = Input.GetKeyDown(key);
        return rval;
    }
}
