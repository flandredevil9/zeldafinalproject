using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DeathCondition : Condition
{
    [SerializeField] private int health;

    public override bool Eval(GameObject recipient)
    {
        health =  recipient.GetComponent<HealthLogic>().currentHealth;
        //health = el.currentHealth;
        if (health <1)
        {
            return true;
        }
        else
        {
            return false;
        }
    }
}