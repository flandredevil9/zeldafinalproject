using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KeyNotPressedCondition : Condition
{
    [SerializeField] private KeyCode key;
    public override bool Eval(GameObject recipient)
    {
        bool rval = true;
        if(Input.GetKey(key))
        {
            rval = false;
        }
        return rval;
    }
}
