using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CollisionStayCondition : Condition
{
    public override bool Eval(GameObject recipient)
    {

        bool rval = false;
        if (recipient.GetComponent<StayCollision>().hasCollided)
        {
            rval = true;
            recipient.GetComponent<StayCollision>().hasCollided = false;
        }
        return rval;
    }
}
