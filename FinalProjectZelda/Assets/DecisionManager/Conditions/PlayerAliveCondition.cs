using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerAliveCondition : Condition
{
    [SerializeField] bool aliveStatus;
    public override bool Eval(GameObject recipient)
    {
        if(aliveStatus == GameManager.instance.playerAlive)
        {
            return true;
        }
        else
        {
            return false;
        }
    }
}
