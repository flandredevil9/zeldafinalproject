using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TriggerStayCondition : Condition
{
    public override bool Eval(GameObject recipient)
    {
        bool rval = false;
        if (recipient.GetComponent<StayTrigger>().hasTriggered)
        {
            rval = true;
            recipient.GetComponent<StayTrigger>().hasTriggered = false;

        }
        return rval;
    }
}
