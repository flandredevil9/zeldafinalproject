using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class TriggerEnterCondition : Condition
{
    public override bool Eval(GameObject recipient)
    {
        bool rval = false;
        if(recipient.GetComponent<EnterTrigger>().hasTriggered)
        {
            rval = true;
            recipient.GetComponent<EnterTrigger>().hasTriggered = false;

        }
        return rval;
    }
}