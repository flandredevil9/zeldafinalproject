using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TakeDamageCondition : Condition
{
    public override bool Eval(GameObject recipient)
    {
        int currentHealth = recipient.GetComponent<HealthLogic>().currentHealth;
        int maxHealth = recipient.GetComponent<HealthLogic>().maxHealth;
        bool rval = false;
        if (currentHealth == maxHealth)
        {
            rval = true;
        }
        return rval;
    }

}
