using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KeyUpCondition : Condition
{
    [SerializeField] private KeyCode key;
    public override bool Eval(GameObject recipient)
    {
        bool rval = Input.GetKeyUp(key);
        return rval;
    }
}
