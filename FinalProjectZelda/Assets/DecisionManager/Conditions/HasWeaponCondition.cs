using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HasWeaponCondition : Condition
{
    public override bool Eval(GameObject recipient)
    {
        bool rval = recipient.GetComponent<PlayerLogic>().hasWeapon;
        return rval;
    }
}
