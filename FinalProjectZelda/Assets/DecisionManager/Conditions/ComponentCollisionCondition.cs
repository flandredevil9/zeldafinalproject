using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

public class ComponentCollisionCondition : Condition
{
    [SerializeField] public GameObject prefab;
    public override bool Eval(GameObject recipient)
    {
        //Triggers

        bool rval = default;
        foreach(Collider obj in recipient.GetComponent<EnterTrigger>().objColliders)
        {
           Debug.Log("current GO " + recipient + " | " + prefab.name + " Prefab | Physical Trigger | Collider " + obj.gameObject.name);
            if (obj.gameObject.name == prefab.name)
            {

                Debug.Log("Collision has happened " + prefab.name);
                rval = true;
            }
        }

        recipient.GetComponent<EnterTrigger>().objColliders.Clear();

        return rval;
    }
}
