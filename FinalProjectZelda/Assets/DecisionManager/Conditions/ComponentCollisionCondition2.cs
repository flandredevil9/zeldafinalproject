using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ComponentCollisionCondition2 : Condition
{
    [SerializeField] public GameObject prefab;
    public override bool Eval(GameObject recipient)
    {
        //Collision objCollider = recipient.GetComponent<EnterCollision>().objCollision;
        Debug.Log(recipient + " physical collision");
        bool rval = default;
        if (recipient.GetComponent<EnterCollision>().objCollision.gameObject.name == prefab.name)
        {
            Debug.Log("Nice! Collisions are equal!!" + prefab.name);
            rval = true;
        }

        return rval;
    }
}
