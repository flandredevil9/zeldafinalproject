using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Data", menuName ="ScriptableObjects/LogicSequenceSO", order = 1)]

public class LogicSequenceSO : ScriptableObject
{
    public List<Expression> expressions;
    public void Evaluate(GameObject recipient)
    {
        foreach(Expression e in expressions)
        {
            e.Eval(recipient);
        }
    }
}
