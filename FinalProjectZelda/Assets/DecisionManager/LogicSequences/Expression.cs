using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Expression
{
    [SerializeField, Tooltip("brief note about this expression's role in the logic sequence")] private string description;
    [SerializeField] private List <Condition> _conditions;
    [SerializeField] private List <Response> _responses;

    public bool Eval(GameObject recipient)
    {
        bool rval = false;

        foreach (Condition condition in _conditions)
        {
            if (condition)
            {
                bool result = condition.Eval(recipient);

                if(result == false)
                {
                    rval = false;
                    break;
                }
                else if(result == true)
                {
                    rval = true;
                }
            }
        }
        if (rval == true)
        {
            foreach(Response r in _responses)
            {
                r.Dispatch(recipient);
            }            

        }
        return rval;
    }
}
