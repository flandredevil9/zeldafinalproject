using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TranslateRandomResponse : Response
{
    public override void Dispatch(GameObject recipient)
    {
        //Fake randomness will fall into pattern but will not go out of bounds
        int random = Random.Range(1, 4); 
        //while(random == recipient.GetComponent<EnemyLogic>().currentDirection 
        //    || random == recipient.GetComponent<EnemyLogic>().lastDirection1
        //    || random == recipient.GetComponent<EnemyLogic>().lastDirection2)
        //{
        //    random = Random.Range(1, 5);
        //}
        //Update directions
        //recipient.GetComponent<EnemyLogic>().lastDirection2 = recipient.GetComponent<EnemyLogic>().lastDirection1;
        //recipient.GetComponent<EnemyLogic>().lastDirection1 = recipient.GetComponent<EnemyLogic>().currentDirection;
        //recipient.GetComponent<EnemyLogic>().currentDirection = random;
        switch (random)
        {
            case 1:
                //Down
                recipient.transform.root.localRotation = Quaternion.Euler(0,180,0);
                break;
            case 2:
                //left
                recipient.transform.root.localRotation = Quaternion.Euler(0,-90,0);
                break;
            case 3:
                //right
                recipient.transform.root.localRotation = Quaternion.Euler(0,90,0);
                break;
            case 4:
                //up
                recipient.transform.root.localRotation = Quaternion.Euler(0,0,0);
                break;
            default:
                break;
        }
    }

}
