using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TakeDamageResponse : Response
{
    public override void Dispatch(GameObject recipient)
    {
        Debug.Log("Damage Taken: " + recipient.name);
        recipient.GetComponent<HealthLogic>().TakeDamage(1);
        //Destroy(recipient);
        //recipient.transform.root.Translate(speed * Time.deltaTime * displacement, Space.World);
    }
}
