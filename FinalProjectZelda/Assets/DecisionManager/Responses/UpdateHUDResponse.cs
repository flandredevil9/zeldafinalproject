using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class UpdateHUDResponse : Response
{
    //[SerializeField] Text health;
    //[SerializeField] Text score;
    public override void Dispatch(GameObject recipient)
    {
        //Text test = GetComponentInChildren<>
        Text health = recipient.transform.Find("HUDPanel/Health").GetComponent<Text>();
        Text score = recipient.transform.Find("HUDPanel/Score").GetComponent<Text>();
        int healthVal =  GameObject.Find("Player").GetComponent<HealthLogic>().currentHealth;

        //TextMeshPro health = recipient.GetComponent<TextMeshPro>();
        health.text = "Health: " + healthVal;
        score.text = "Score: " + GameManager.instance.GetScore();
        
        //healt.text = ("wow");
        
        //recipient.transform.root.LookAt(rotation);
        //recipient.transform.root.localRotation = Quaternion.Euler(rotation);
        //Rotate(rotation);
        //recipient.transform.root.Translate(speed * Time.deltaTime * displacement);
    }
}
