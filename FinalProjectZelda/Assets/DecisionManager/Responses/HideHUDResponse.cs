using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HideHUDResponse : Response
{
    public override void Dispatch(GameObject recipient)
    {
        GameObject HUD = recipient.transform.Find("HUDPanel").gameObject;

        HUD.SetActive(false);
        //GameManager.instance.playerAlive = aliveStatus;
        //Debug.Log("Alive Status: " + GameManager.instance.playerAlive);
    }
}
