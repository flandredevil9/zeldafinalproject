using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SwordAttackResponse : Response
{
    [SerializeField] public GameObject prefab;
    public override void Dispatch(GameObject recipient)
    {
        GameObject go = Instantiate(prefab, recipient.transform.Find("LArm/Hand").transform.position, Quaternion.Euler(90, recipient.transform.rotation.eulerAngles.y, 0));
        go.name = prefab.name;
    }
}
