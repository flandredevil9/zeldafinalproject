using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroyResponse : Response
{
    //[SerializeField] private Vector3 displacement;
    //[SerializeField] private float speed;
    //[SerializeField] public Transform objectToDestroy;
    public override void Dispatch(GameObject recipient)
    {
        Debug.Log("Destroy: " + recipient.name);
        Destroy(recipient);
        //recipient.transform.root.Translate(speed * Time.deltaTime * displacement, Space.World);
    }
}
