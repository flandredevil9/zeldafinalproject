using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FullHPAttackResponse : Response
{
    [SerializeField] public GameObject prefab;
    public override void Dispatch(GameObject recipient)
    {
        
        GameObject swordProjectile = Instantiate(prefab, recipient.transform.Find("LArm/Hand").transform.position, Quaternion.Euler(90, recipient.transform.rotation.eulerAngles.y, 0));
        swordProjectile.name = prefab.name;
        
            //p.velocity = transform.forward * speed;
            //Instantiate(prefab, recipient.transform);

    }
}
