using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TranslateResponse : Response
{
    [SerializeField] private Vector3 displacement;
    [SerializeField] private float speed;

    public override void Dispatch(GameObject recipient)
    {
        recipient.transform.root.Translate(speed * Time.deltaTime * displacement, Space.World);
    }
}
