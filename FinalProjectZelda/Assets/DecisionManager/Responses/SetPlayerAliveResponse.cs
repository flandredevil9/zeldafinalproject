using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SetPlayerAliveResponse : Response
{
    [SerializeField] bool aliveStatus;
    public override void Dispatch(GameObject recipient)
    {
        GameManager.instance.playerAlive = aliveStatus;
        Debug.Log("Alive Status: " + GameManager.instance.playerAlive);
    }
}
