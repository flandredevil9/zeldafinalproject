using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroySpecificObjectResponse : Response
{
    [SerializeField] public Transform objectToDestroy;
    public override void Dispatch(GameObject recipient)
    {
        Debug.Log("Destroy: " + recipient.name);
        //Destroy(recipient.GetComponent<>());
        //recipient.transform.root.Translate(speed * Time.deltaTime * displacement, Space.World);
    }
}
