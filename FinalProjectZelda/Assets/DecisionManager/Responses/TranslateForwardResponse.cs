using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TranslateForwardResponse : Response
{

    [SerializeField] private float speed;

    public override void Dispatch(GameObject recipient)
    {
        recipient.transform.root.Translate(speed * Time.deltaTime * transform.up);
    }
}
