using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TurnResponse : Response
{
    [SerializeField] private Vector3 rotation;

    public override void Dispatch(GameObject recipient)
    {

        //recipient.transform.root.LookAt(rotation);
        recipient.transform.root.localRotation = Quaternion.Euler(rotation);
            //Rotate(rotation);
        //recipient.transform.root.Translate(speed * Time.deltaTime * displacement);
    }
}