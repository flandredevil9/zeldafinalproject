using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ShowGameOverResponse : Response
{
    public override void Dispatch(GameObject recipient)
    {
        GameObject HUD = recipient.transform.Find("GameOverPanel").gameObject;
        HUD.SetActive(true);

        Text score = recipient.transform.Find("GameOverPanel/FinalScore").GetComponent<Text>();
        score.text = "Final Score: " + GameManager.instance.GetScore();

        //GameManager.instance.playerAlive = aliveStatus;
        //Debug.Log("Alive Status: " + GameManager.instance.playerAlive);
    }
}
